// Set up dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Allows access to routes defined within "userRoute.js"
const userRoute = require("./routes/userRoute");
// Allows access to routes defined within "courseRoute.js"
const courseRoute = require("./routes/courseRoute");

// Server
const app = express();

// Middleware
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));



// Allows all the user routes created in "userRoute.js" file to use "/users" as route (resources)
app.use("/users", userRoute);
// Allows all the course routes created in "courseRoute.js" file to use "/courses" as route (resources)
app.use("/courses", courseRoute);



// Database Connection
mongoose.connect("mongodb+srv://natquinio:admin123@zuitt-bootcamp.77elyq8.mongodb.net/courseBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.once("open", () => console.log('Now connected to cloud database!'));


// Server listening
// Will use the defined port number for the application whenever environment variable is available or use port 4000 if none in defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));